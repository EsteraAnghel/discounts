package com.eanghel.service;

import com.eanghel.data.dto.DiscountAgreementDTO;
import com.eanghel.data.dto.ProductDTO;
import com.eanghel.data.entry.DiscountAgreement;
import com.eanghel.data.entry.Product;
import com.eanghel.data.repository.DiscountAgreementRepository;
import com.eanghel.data.repository.ProductRepository;
import com.eanghel.entityUtils.EntityUtils;
import com.eanghel.forms.PriceCalcForm;
import com.eanghel.forms.ResultForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class ProductService {
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private DiscountAgreementRepository discountAgreementRepository;

    @Transactional
    public List<ProductDTO> findAll() {
        List<ProductDTO> productDTOList = new ArrayList<>();
        List<Product> productList = productRepository.findAll();
        Double startingPrice;
        ProductDTO productDTO;
        for (Product product : productList) {
            productDTO = EntityUtils.productToProductDTO(product);
            productDTOList.add(productDTO);
        }
        return productDTOList;
    }

    public List<ProductDTO> productsListToProductDTOList(List<Product> products) {
        List<ProductDTO> productsListToProductDTOList = new ArrayList<ProductDTO>();
        if (Objects.isNull(products))
            for (Product product : products) {
                productsListToProductDTOList.add(EntityUtils.productToProductDTO(product));
            }
        return productsListToProductDTOList;
    }

    public ResultForm calculatePrice(PriceCalcForm priceCalcForm) {
        ResultForm resultForm = new ResultForm();
        resultForm.setDiscount(0);
        Product product=productRepository.findById(priceCalcForm.getProductId()).orElse(null);
        if(Objects.nonNull(product)){
        Discounter discounter = new DiscounterImpl();
        Double price = 0.0;
        resultForm.setOldPrice(product.getPrice());
        for (Product prod : productRepository.findAll()) {
            if (prod.getId().equals(product.getId()))
                price = prod.getPrice();
        }

        DiscountAgreementDTO discountAgreementForProduct = new DiscountAgreementDTO();
        List<DiscountAgreement> discountAgreements = discountAgreementRepository.findByClientId(priceCalcForm.getClientID());
        for (DiscountAgreement discountAgreement : discountAgreements) {
            if (discountAgreement.getProductId().equals(product.getId()) ) {
                discountAgreementForProduct = EntityUtils.discountAgreementToDiscountAgereementDTO(discountAgreement);
                resultForm.setDiscount(discountAgreement.getDiscount());

            }
            if (priceCalcForm.getQuantity() >= discountAgreementForProduct.getQuantity()) {
                if(Objects.isNull(product.getDiscount()) || product.getDiscount() <= discountAgreement.getDiscount()){
                price=discounter.applyDiscount(BigDecimal.valueOf(product.getPrice()), discountAgreementForProduct.getDiscount()).doubleValue();
                product.setPrice(price);
                resultForm.setActualPrice(price);}
                else {
                    price=discounter.applyDiscount(BigDecimal.valueOf(product.getPrice()), product.getDiscount()).doubleValue();
                    product.setPrice(price);
                    resultForm.setActualPrice(price);
                }
            } else {
                product.setPrice(price);
                resultForm.setActualPrice(price);
            }
        }
        return resultForm;}
        else return resultForm;
    }
}
