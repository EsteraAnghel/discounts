package com.eanghel.service;

import java.math.BigDecimal;

public class DiscounterImpl implements Discounter {
    @Override
    public BigDecimal applyDiscount(BigDecimal amount,Integer discount) {
        return amount.subtract(BigDecimal.valueOf(discount).divide(BigDecimal.valueOf(100)).multiply(amount));

    }
}
