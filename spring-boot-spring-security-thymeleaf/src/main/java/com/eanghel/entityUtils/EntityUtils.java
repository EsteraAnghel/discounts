package com.eanghel.entityUtils;

import com.eanghel.data.dto.*;
import com.eanghel.data.entry.*;

public class EntityUtils {
    public static ClientDTO clientToClientDTO(Client client) {
        return new ClientDTO.Builder()
                .id(client.getId())
                .firstName(client.getFirstName())
                .lastName(client.getLastName())
                .email(client.getEmail())
                .address(client.getAddress())
                .phone(client.getPhone())
                .discountAgreements(client.getDiscountAgreements())
                .create();
    }

    public static Client clientDTOToClient(ClientDTO clientDTO) {
        return new Client.Builder()
                .id(clientDTO.getId())
                .firstName(clientDTO.getFirstName())
                .lastName(clientDTO.getLastName())
                .email(clientDTO.getEmail())
                .address(clientDTO.getAddress())
                .phone(clientDTO.getPhone())
                .discountAgreements(clientDTO.getDiscountAgreements())
                .create();
    }

    public static DiscountAgreement discountAgreementDTOToDiscountAgreement(DiscountAgreementDTO discountAgreementDTO) {
        return new DiscountAgreement.Builder()
                .id(discountAgreementDTO.getId())
                .clientId(discountAgreementDTO.getClientId())
                .quantity(discountAgreementDTO.getQuantity())
                .discount(discountAgreementDTO.getDiscount())
                .productCategory(discountAgreementDTO.getProductCategory())
                .productStandard(discountAgreementDTO.getProductStandard())
                .create();
    }

    public static DiscountAgreementDTO discountAgreementToDiscountAgereementDTO(DiscountAgreement discountAgreement) {
        return new DiscountAgreementDTO.Builder()
                .id(discountAgreement.getId())
                .clientId(discountAgreement.getClientId())
                .quantity(discountAgreement.getQuantity())
                .discount(discountAgreement.getDiscount())
                .productCategory(discountAgreement.getProductCategory())
                .productStandard(discountAgreement.getProductStandard())
                .create();
    }

    public static Product productDTOToProduct(ProductDTO productDTO) {
        return new Product.Builder()
                .id(productDTO.getId())
                .description(productDTO.getDescription())
                .discount(productDTO.getDiscount())
                .name(productDTO.getName())
                .stoc(productDTO.getStoc())
                .price(productDTO.getPrice())
                .productCategory(productDTO.getProductCategory())
                .productStandard(productDTO.getProductStandard())
                .create();
    }

    public static ProductDTO productToProductDTO(Product product) {
        return new ProductDTO.Builder()
                .id(product.getId())
                .description(product.getDescription())
                .discount(product.getDiscount())
                .name(product.getName())
                .stoc(product.getStoc())
                .price(product.getPrice())
                .productCategory(product.getProductCategory())
                .productStandard(product.getProductStandard())
                .create();
    }

    public static ProductCategory productCategoryDTOToProduct(ProductCategoryDTO productCategoryDTO) {
        return new ProductCategory.Builder()
                .id(productCategoryDTO.getId())
                .category(productCategoryDTO.getCategory())
                .maxDiscount(productCategoryDTO.getMaxDiscount())
                .create();
    }

    public static ProductCategoryDTO productCategoryToProductCategoryDTO(ProductCategory productCategory) {
        return new ProductCategoryDTO.Builder()
                .id(productCategory.getId())
                .category(productCategory.getCategory())
                .maxDiscount(productCategory.getMaxDiscount())
                .create();
    }

    public static ProductStandard productStandardDTOToProductStandard(ProductStandardDTO productStandardDTO) {
        return new ProductStandard.Builder()
                .id(productStandardDTO.getId())
                .standard(productStandardDTO.getStandard())
                .maxDiscount(productStandardDTO.getMaxDiscount())
                .create();
    }

    public static ProductStandardDTO productStandardToProductStandardDTO(ProductStandard productStandard) {
        return new ProductStandardDTO.Builder()
                .id(productStandard.getId())
                .standard(productStandard.getStandard())
                .maxDiscount(productStandard.getMaxDiscount())
                .create();
    }
}
