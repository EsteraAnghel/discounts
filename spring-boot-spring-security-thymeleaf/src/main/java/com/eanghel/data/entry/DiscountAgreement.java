package com.eanghel.data.entry;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity(name = "discount_agreement")
public class DiscountAgreement {
    @Id
    @Column(name = "id_discount_agreement")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "id_client")
    private Integer clientId;
    @Column(name = "id_product")
    private Integer productId;

    @Column(name = "quantity")
    private Integer quantity;

    @Column(name = "discount")
    private Integer discount;

    @OneToOne
    @JoinColumn(name = "product_category")
    private ProductCategory productCategory;

    @OneToOne
    @JoinColumn(name = "product_standard")
    private ProductStandard productStandard;

    public DiscountAgreement() {
    }

    public DiscountAgreement(Integer id, Integer clientId, Integer quantity, Integer discount, ProductCategory productCategory, ProductStandard productStandard, Integer productId) {
        this.id = id;
        this.clientId = clientId;
        this.quantity = quantity;
        this.discount = discount;
        this.productCategory = productCategory;
        this.productStandard = productStandard;
        this.productId = productId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getClientId() {
        return clientId;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public ProductCategory getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(ProductCategory productCategory) {
        this.productCategory = productCategory;
    }

    public ProductStandard getProductStandard() {
        return productStandard;
    }

    public void setProductStandard(ProductStandard productStandard) {
        this.productStandard = productStandard;
    }

    public static class Builder {
        private Integer nestedId;
        private Integer nestedClientId;
        private Integer nestedQuantity;
        private Integer nestedDiscount;
        private ProductCategory nestedProductCategory;
        private ProductStandard nestedProductStandard;
        private Integer nestedProductID;
        public Builder productId(Integer id) {
            this.nestedProductID = id;
            return this;
        }

        public Builder id(Integer id) {
            this.nestedId = id;
            return this;
        }

        public Builder clientId(Integer clientId) {
            this.nestedClientId = clientId;
            return this;
        }

        public Builder quantity(Integer quantity) {
            this.nestedQuantity = quantity;
            return this;
        }

        public Builder discount(Integer discount) {
            this.nestedDiscount = discount;
            return this;
        }

        public Builder productStandard(ProductStandard productStandard) {
            this.nestedProductStandard = productStandard;
            return this;
        }

        public Builder productCategory(ProductCategory productCategory) {
            this.nestedProductCategory = productCategory;
            return this;
        }

        public DiscountAgreement create() {
            return new DiscountAgreement(nestedId, nestedClientId, nestedQuantity, nestedDiscount, nestedProductCategory, nestedProductStandard,nestedProductID);
        }
    }
}
