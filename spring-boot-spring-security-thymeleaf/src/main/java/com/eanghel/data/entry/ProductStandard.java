package com.eanghel.data.entry;

import javax.persistence.*;


@Entity(name = "product_standard")
public class ProductStandard {

    @Id
    @Column(name = "id_product_standard")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "standard")
    private String standard;
    @Column(name = "max_discount")
    private Integer maxDiscount;


    public ProductStandard() {
    }

    public ProductStandard(Integer id, String standard, Integer maxDisc) {
        this.id = id;
        this.standard = standard;
        this.maxDiscount =maxDisc;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMaxDiscount() {
        return maxDiscount;
    }

    public void setMaxDiscount(Integer maxDiscount) {
        this.maxDiscount = maxDiscount;
    }

    public String getStandard() {
        return standard;
    }

    public void setStandard(String standard) {
        this.standard = standard;
    }

    public static class Builder {
        private Integer nestedId;
        private String nestedStandard;
        private Integer nestedMaxDiscount;

        public Builder id(Integer id) {
            this.nestedId = id;
            return this;
        }

        public Builder standard(String standard) {
            this.nestedStandard = standard;
            return this;
        }
        public Builder maxDiscount(Integer maxDisc) {
            this.nestedMaxDiscount = maxDisc;
            return this;
        }

        public ProductStandard create() {
            return new ProductStandard(nestedId, nestedStandard, nestedMaxDiscount);
        }
    }
}
