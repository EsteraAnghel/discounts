package com.eanghel.data.entry;

import javax.persistence.*;


@Entity(name = "product_category")
public class ProductCategory {

    @Id
    @Column(name = "id_product_category")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "category")
    private String category;
    @Column(name = "max_discount")
    private Integer maxDiscount;


    public ProductCategory() {
    }


    public ProductCategory(Integer id, String category, Integer maxDisc) {
        this.id = id;
        this.category = category;
        this.maxDiscount=maxDisc;
    }
    public Integer getMaxDiscount() {
        return maxDiscount;
    }

    public void setMaxDiscount(Integer maxDiscount) {
        this.maxDiscount = maxDiscount;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public static class Builder {
        private Integer nestedId;
        private String nestedCategory;
        private Integer nestedMaxDiscount;


        public Builder id(Integer id) {
            this.nestedId = id;
            return this;
        }
        public Builder maxDiscount(Integer maxDisc) {
            this.nestedMaxDiscount = maxDisc;
            return this;
        }

        public Builder category(String category) {
            this.nestedCategory = category;
            return this;
        }

        public ProductCategory create() {
            return new ProductCategory(nestedId, nestedCategory,nestedMaxDiscount);
        }
    }
}
