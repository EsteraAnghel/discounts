package com.eanghel.data.entry;

import javax.persistence.*;
import java.util.Objects;

@Entity(name = "product")
public class Product {


    @Id
    @Column(name = "id_product")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "discount")
    private Integer discount;

    @Column(name = "stoc")
    private Integer stoc;

    @Column(name = "price")
    private Double price;

    @ManyToOne
    @JoinColumn(name = "id_product_category")
    private ProductCategory productCategory;
    @ManyToOne
    @JoinColumn(name = "id_product_standard")
    private ProductStandard productStandard;

    public Product() {
    }


    public Product(Integer id, ProductCategory category, String name, String description, Integer discount, Double price, Integer stoc, ProductStandard productStandard) {
        this.id = id;
        this.productCategory = category;
        this.description = description;
        this.name = name;
        this.discount = discount;
        this.stoc = stoc;
        this.price = price;
        this.productStandard=productStandard;

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ProductCategory getCategory() {
        return productCategory;
    }

    public void setCategory(ProductCategory category) {
        this.productCategory = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public Integer getStoc() {
        return stoc;
    }

    public void setStoc(Integer stoc) {
        this.stoc = stoc;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public ProductCategory getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(ProductCategory productCategory) {
        this.productCategory = productCategory;
    }

    public ProductStandard getProductStandard() {
        return productStandard;
    }

    public void setProductStandard(ProductStandard productStandard) {
        this.productStandard = productStandard;
    }

    public static class Builder {
        private Integer nestedId;
        private ProductCategory nestedProductCategory;
        private String nestedName;
        private String nestedDescription;
        private Integer nestedDiscount;
        private Integer nestedStoc;
        private Double nestedPrice;
        private ProductStandard nestedProductStandard;

        public Builder price(Double price) {
            if (Objects.isNull(price)) {
                this.nestedPrice = 0.0;
            } else {
                this.nestedPrice = price;
            }
            return this;
        }

        public Builder stoc(Integer stoc) {
            if (Objects.isNull(stoc)) {
                this.nestedStoc = 0;
            } else {
                this.nestedStoc = stoc;
            }
            return this;
        }

        public Builder discount(Integer discount) {
            if (Objects.isNull(discount)) {
                this.nestedDiscount = 0;
            } else {
                this.nestedDiscount = discount;
            }
            return this;
        }

        public Builder id(Integer id) {
            this.nestedId = id;
            return this;
        }


        public Builder name(String name) {
            this.nestedName = name;
            return this;
        }


        public Builder description(String description) {
            this.nestedDescription = description;
            return this;
        }

        public Builder productCategory(ProductCategory productCategory) {
            this.nestedProductCategory = productCategory;
            return this;
        }
        public Builder productStandard(ProductStandard productStandard) {
            this.nestedProductStandard = productStandard;
            return this;
        }


        public Product create() {
            return new Product(nestedId, nestedProductCategory, nestedName, nestedDescription, nestedDiscount, nestedPrice, nestedStoc, nestedProductStandard);
        }

    }
}


