package com.eanghel.data.repository;

import com.eanghel.data.entry.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {

    //List<Product> findId(Integer Id);
    Optional<Product> findById(Integer id);




}
