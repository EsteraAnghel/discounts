package com.eanghel.data.repository;

import com.eanghel.data.entry.DiscountAgreement;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


@Repository
public interface DiscountAgreementRepository extends JpaRepository<DiscountAgreement, Integer> {
    List<DiscountAgreement> findByClientId(Integer clientId);

}
