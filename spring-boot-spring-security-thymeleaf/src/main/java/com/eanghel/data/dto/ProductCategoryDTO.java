package com.eanghel.data.dto;

import javax.persistence.*;


public class ProductCategoryDTO {
    private Integer id;
    private String category;
    private Integer maxDiscount;

    public ProductCategoryDTO() {
    }

    public ProductCategoryDTO(Integer id, String category, Integer maxDiscount) {
        this.id = id;
        this.category = category;
        this.maxDiscount = maxDiscount;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public Integer getMaxDiscount() {
        return maxDiscount;
    }

    public void setMaxDiscount(Integer maxDiscount) {
        this.maxDiscount = maxDiscount;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public static class Builder {
        private Integer nestedId;
        private String nestedCategory;
        private Integer nestedMaxDiscount;

        public Builder id(Integer id) {
            this.nestedId = id;
            return this;
        }
        public Builder maxDiscount(Integer maxDiscount) {
            this.nestedMaxDiscount = maxDiscount;
            return this;
        }
        public Builder category(String category) {
            this.nestedCategory = category;
            return this;
        }

        public ProductCategoryDTO create() {
            return new ProductCategoryDTO(nestedId, nestedCategory,nestedMaxDiscount);
        }
    }
}
