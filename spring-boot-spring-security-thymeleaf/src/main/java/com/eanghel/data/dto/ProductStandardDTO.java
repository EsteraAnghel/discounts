package com.eanghel.data.dto;

public class ProductStandardDTO {

    private Integer id;
    private String standard;
    private Integer maxDiscount;



    public ProductStandardDTO() {
    }

    public ProductStandardDTO(Integer id, String standard, Integer maxDisc) {
        this.id = id;
        this.standard = standard;
        this.maxDiscount=maxDisc;
    }

    public Integer getMaxDiscount() {
        return maxDiscount;
    }

    public void setMaxDiscount(Integer maxDiscount) {
        this.maxDiscount = maxDiscount;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStandard() {
        return standard;
    }

    public void setStandard(String standard) {
        this.standard = standard;
    }

    public static class Builder {
        private Integer nestedId;
        private String nestedStandard;
        private Integer nestedMaxDiscount;

        public Builder id(Integer id) {
            this.nestedId = id;
            return this;
        }

        public Builder standard(String standard) {
            this.nestedStandard = standard;
            return this;
        }
        public Builder maxDiscount(Integer maxDiscount) {
            this.nestedMaxDiscount = maxDiscount;
            return this;
        }

        public ProductStandardDTO create() {
            return new ProductStandardDTO(nestedId, nestedStandard, nestedMaxDiscount);
        }
    }
}
