package com.eanghel.data.dto;

import com.eanghel.data.entry.DiscountAgreement;
import com.eanghel.data.entry.ProductCategory;
import com.eanghel.data.entry.ProductStandard;

public class DiscountAgreementDTO {

    private Integer id;
    private Integer clientId;
    private Integer quantity;
    private Integer discount;
    private ProductCategory productCategory;
    private ProductStandard productStandard;
    private Integer productId;

    public DiscountAgreementDTO() {
    }

    public DiscountAgreementDTO(Integer id, Integer clientId, Integer quantity, Integer discount, ProductCategory productCategory, ProductStandard productStandard, Integer productId) {
        this.id = id;
        this.clientId = clientId;
        this.quantity = quantity;
        this.discount = discount;
        this.productCategory = productCategory;
        this.productStandard = productStandard;
        this.productId=productId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getClientId() {
        return clientId;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public ProductCategory getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(ProductCategory productCategory) {
        this.productCategory = productCategory;
    }

    public ProductStandard getProductStandard() {
        return productStandard;
    }

    public void setProductStandard(ProductStandard productStandard) {
        this.productStandard = productStandard;
    }

    public static class Builder {
        private Integer nestedId;
        private Integer nestedClientId;
        private Integer nestedQuantity;
        private Integer nestedDiscount;
        private ProductCategory nestedProductCategory;
        private ProductStandard nestedProductStandard;

        private Integer nestedProductID;
        public Builder productId(Integer id) {
            this.nestedProductID = id;
            return this;
        }
        public Builder id(Integer id) {
            this.nestedId = id;
            return this;
        }

        public Builder clientId(Integer clientId) {
            this.nestedClientId = clientId;
            return this;
        }

        public Builder quantity(Integer quantity) {
            this.nestedQuantity = quantity;
            return this;
        }

        public Builder discount(Integer discount) {
            this.nestedDiscount = discount;
            return this;
        }

        public Builder productStandard(ProductStandard productStandard) {
            this.nestedProductStandard = productStandard;
            return this;
        }

        public Builder productCategory(ProductCategory productCategory) {
            this.nestedProductCategory = productCategory;
            return this;
        }

        public DiscountAgreementDTO create() {
            return new DiscountAgreementDTO(nestedId, nestedClientId, nestedQuantity, nestedDiscount, nestedProductCategory, nestedProductStandard, nestedProductID);
        }
    }
}
