package com.eanghel.data.dto;

import com.eanghel.data.entry.DiscountAgreement;

import java.sql.Date;
import java.util.Set;


public class ClientDTO {

    private Integer id;
    private String firstName;
    private String lastName;
    private Date birthDate;
    private String email;
    private String address;


    private String phone;

    private Set<DiscountAgreement> discountAgreements;

    public ClientDTO() {
    }

    public ClientDTO(Integer id, String firstName, String lastName, Date birthDate, String email, String address, String phone, Set<DiscountAgreement> discountAgreements) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.email = email;
        this.address = address;
        this.phone = phone;
        this.discountAgreements = discountAgreements;


    }

    public Set<DiscountAgreement> getDiscountAgreements() {
        return discountAgreements;
    }

    public void setDiscountAgreements(Set<DiscountAgreement> discountAgreements) {
        this.discountAgreements = discountAgreements;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }


    public static class Builder {
        private Integer nestedId;
        private String nestedFirstName;
        private String nestedLastName;
        private Date nestedBirthDate;
        private String nestedEmail;
        private String nestedAddress;
        private String nestedPhone;

        private Set<DiscountAgreement> nestedDiscountAgreements;

        public Builder discountAgreements(Set<DiscountAgreement> discountAgreements) {
            this.nestedDiscountAgreements = discountAgreements;
            // this.nestedEventCategories.forEach(x -> x.getClients().add(this));
            return this;
        }

        public Builder id(Integer id) {
            this.nestedId = id;
            return this;
        }

        public Builder firstName(String firstName) {
            this.nestedFirstName = firstName;
            return this;
        }

        public Builder lastName(String lastName) {
            this.nestedLastName = lastName;
            return this;
        }

        public Builder birthDate(Date birthDate) {
            this.nestedBirthDate = birthDate;
            return this;
        }

        public Builder email(String email) {
            this.nestedEmail = email;
            return this;
        }

        public Builder address(String address) {
            this.nestedAddress = address;
            return this;
        }

        public Builder phone(String phone) {
            this.nestedPhone = phone;
            return this;
        }

        public ClientDTO create() {
            return new ClientDTO(nestedId, nestedFirstName, nestedLastName, nestedBirthDate, nestedEmail, nestedAddress, nestedPhone, nestedDiscountAgreements);
        }
    }
}
