package com.eanghel.controller;

import com.eanghel.data.dto.ProductDTO;
import com.eanghel.forms.PriceCalcForm;
import com.eanghel.forms.ResultForm;
import com.eanghel.service.ProductService;
import com.sun.xml.internal.messaging.saaj.packaging.mime.MessagingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;
import java.util.List;

@Controller
public class ProductController {
    @Autowired
    private ProductService productService;


    @GetMapping("/admin")
    public ModelAndView viewAllEvents(Principal principal) {
        List<ProductDTO> productDTOSList = productService.findAll();
        ModelAndView modelAndView = new ModelAndView("admin");
        PriceCalcForm priceCalcForm = new PriceCalcForm();
        modelAndView.addObject("priceCalcForm", priceCalcForm);
        modelAndView.addObject("productDTOSList", productDTOSList);

        return modelAndView;}


    @PostMapping("/priceCalcForm")
    public String registerClientPost(@ModelAttribute(value = "priceCalcForm") PriceCalcForm priceCalcForm, Model model) throws MessagingException {
         ModelAndView mav = new ModelAndView();
         ResultForm resultForm= productService.calculatePrice(priceCalcForm);
       model.addAttribute("resultForm", resultForm);
        System.out.println(priceCalcForm);
        model.addAttribute("priceCalcForm", priceCalcForm);

      //  mav.setViewName("result");
        return "result";
    }
//    @PostMapping(value ="/priceCalcForm")
//    public ModelAndView submitEmployee(@ModelAttribute PriceCalcForm priceCalcForm){
//        ModelAndView mav = new ModelAndView();
//
//        System.out.println(priceCalcForm);
//      //  employeeService.saveEmployee(employee);
//
//        mav.setViewName("redirect:/admin");
//        return mav;
//
//    }

}
